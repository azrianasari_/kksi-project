@extends('layouts.main')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            {{-- <h1>Validation</h1> --}}
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Data <small>Siswa</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm" action="{{route('siswa.store')}}" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nisn">NISN</label>
                    <input type="text" name="nisn" class="form-control rounded-0" id="nisn" placeholder=" Enter NISN" required>
                  </div>
                  <div class="form-group">
                    <label for="nis">NIS</label>
                    <input type="text" name="nis" class="form-control rounded-0" id="nis" placeholder=" Enter NIS" required>
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control rounded-0" id="nama" placeholder=" Enter Nama" required>
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" name="alamat" class="form-control rounded-0" id="alamat" placeholder=" Enter Alamat" required>
                  </div>
                  <div class="form-group">
                    <label for="no_telp">Nomor Telepon</label>
                    <input type="text" name="no_telp" class="form-control rounded-0" id="no_telp" placeholder=" Enter Nomor Telepon" required>
                  </div>
                  <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <select class="form-control select2 rounded-0" style="width: 100%;" id="kelas" name="kelas" required>
                          <option value = "">-- pilih --</option>
                          <option value = "1">X RPL</option>
                          <option value = "2">X TKJ</option>
                          <option value = "3">X MM</option>
                          <option value = "4">XI RPL</option>
                          <option value = "5">XI TKJ</option>
                          <option value = "6">XI MM</option>
                          <option value = "7">XII RPL</option>
                          <option value = "8">XII TKJ</option>
                          <option value = "9">XII MM</option>
                        </select>
                   </div>
                  {{-- <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div> --}}
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-right">
                  <button type="submit" class="btn btn-primary rounded-0">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
    
@endsection