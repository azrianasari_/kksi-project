@extends('layouts.main')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          {{-- <h1>Buttons</h1> --}}
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Daftar Siswa</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <a href="{{route('siswa.create')}}">
                    <i class="fas fa-plus"></i>
                </a>
                Tambah
              </h3>
            </div>


             <!-- /.card-header -->
             <div class="card-body">
               <div class="row">
                  <div class="col-lg-10">
                      <p>Daftar <code>Siswa</code></p>
                  </div>
                  <div class="col-lg-2">
                      <div class="card-tools">
                          <div class="input-group input-group-sm">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
          
                            <div class="input-group-append">
                              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                            </div>      
                          </div>
                        </div>
                  </div>

               </div>

               @include('layouts.flash')
               
              <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>NISN / NIS</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th style="width: 60px"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data as $item)
                  <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$item->nisn}} / {{$item->nis}}</td>
                      <td>{{$item->nama}}</td>
                      <td>
                        {{-- <span class="badge bg-danger">55%</span> --}}
                        {{$item->alamat}}
                      </td>
                      <td>
                          <div class="btn-group">
                              {{-- <a href="{{"siswa/$item->id/edit"}}"class="btn btn-warning btn-sm" >edit</a> --}}
                              <button type="button" class="btn btn-danger btn-flat">Action</button>
                              <button type="button" class="btn btn-danger btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                              </button>   
                                <div class="dropdown-menu" role="menu" style="">
                                  <a class="dropdown-item" href="{{"siswa/$item->id/edit"}}">Edit</a>
                                  <form action={{"/siswa/$item->id"}} method="POST" class="d-inline">
                                    @method('delete')
                                    @csrf
                                      <button type="submit" class="dropdown-item" onclick="return confirm('Yakin?')">Hapus</button>
                                  </form>
                                  <a class="dropdown-item" href="{{"siswa/reset/$item->id"}}">Reset Password</a>
                                </div>
                             
                            </div>
                      </td>
                    </tr>
               
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                {{-- <li class="page-item"><a class="page-link" href="#">&laquo;</a></li> --}}
                {{ $data->links() }}
              </ul>
             
            </div>
            
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- ./row -->

    
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>



@endsection