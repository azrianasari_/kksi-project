@extends('layouts.main')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            {{-- <h1>Validation</h1> --}}
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data <small>Siswa</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="quickForm"  method="POST" action="{{ "/siswa/$data->id" }}">
                @method('put')
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nisn">NISN</label>
                    <input type="text" name="nisn" class="form-control rounded-0" id="nisn" placeholder=" Enter NISN" required value="{{$data->nisn}}">
                  </div>
                  <div class="form-group">
                    <label for="nis">NIS</label>
                    <input type="text" name="nis" class="form-control rounded-0" id="nis" placeholder=" Enter NIS" required value="{{$data->nis}}">
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control rounded-0" id="nama" placeholder=" Enter Nama" required value="{{$data->nama}}">
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" name="alamat" class="form-control rounded-0" id="alamat" placeholder=" Enter Alamat" required value="{{$data->alamat}}">
                  </div>
                  <div class="form-group">
                    <label for="no_telp">Nomor Telepon</label>
                    <input type="text" name="no_telp" class="form-control rounded-0" id="no_telp" placeholder=" Enter Nomor Telepon" required value="{{$data->no_telp}}">
                  </div>
                  <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <select class="form-control select2 rounded-0" style="width: 100%;" id="kelas" name="kelas" required>
                          <option value = "">-- pilih --</option>
                          <option value = "1" @if($data->kelas == "1") selected @endif >X RPL</option>
                          <option value = "2" @if($data->kelas == "2") selected @endif >X TKJ</option>
                          <option value = "3" @if($data->kelas == "3") selected @endif >X MM</option>
                          <option value = "4" @if($data->kelas == "4") selected @endif >XI RPL</option>
                          <option value = "5" @if($data->kelas == "5") selected @endif >XI TKJ</option>
                          <option value = "6" @if($data->kelas == "6") selected @endif >XI MM</option>
                          <option value = "7" @if($data->kelas == "7") selected @endif >XII RPL</option>
                          <option value = "8" @if($data->kelas == "8") selected @endif >XII TKJ</option>
                          <option value = "9" @if($data->kelas == "9") selected @endif >XII MM</option>
                        </select>
                   </div>
                  {{-- <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div> --}}
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-right">
                  <button type="submit" class="btn btn-primary rounded-0">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
    
@endsection