@foreach ($data as $item)
<div class="col-md-3 col-sm-6 col-12">
    <a href="{{route('kelas.show',$item->id)}}">
    <div class="info-box bg-info">
    <span class="info-box-icon"><i class="far fa-bookmark"></i></span>
    <div class="info-box-content">
        @if(strlen($item->mapel->nama) > 20)
        <span class="info-box-text">{{substr($item->mapel->nama,0,20)}} ...</span>
        @else
        <span class="info-box-text">{{$item->mapel->nama}}</span>
        @endif
        <span class="info-box-text">{{$item->pegawai->nama}}</span>
        <span class="info-box-number">{{$item->kelas_formatted}} / {{$item->hari_formatted}}</span>
        <div class="progress">
        <div class="progress-bar" style="width: 70%"></div>
        </div>
        <span class="progress-description">
            Jumlah pertemuan {{$item->daftar_kelas->count()}}
        </span>
    </div>
    <!-- /.info-box-content -->
    </div>
    </a>
    <!-- /.info-box -->
</div>
<!-- /.col -->     
@endforeach