    <div class="modal fade" id="modal-lg-{{$item->id}}">
        <div class="modal-dialog modal-lg">
        <form role="form" id="quickForm" method="POST" action="{{ "/kelas/$item->id" }}">
            @method('put')
            @csrf
                
          <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Pertemuan ke-{{$loop->iteration}}</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                        <div class="col-md-6">
                            <label>Tanggal</label>
                            <div class="form-group">
                                  <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                      <input type="date" name="tanggal" class="form-control datetimepicker-input rounded-0" data-target="#reservationdate" value="{{$item->tanggal}}">
                                      <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                          <div class="input-group-text rounded-0"><i class="fa fa-calendar"></i></div>
                                      </div>
                                  </div>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Jadwal</label>
                            <input type="hidden" name="jadwal_id" value="{{$jadwal->id}}">
                            <input type="text" class="form-control rounded-0" id="exampleInputEmail1" placeholder="Enter email" value="{{$jadwal->hari_formatted}} / {{$jadwal->waktu_awal}} - {{$jadwal->waktu_akhir}} ">
                          </div>
                          <!-- /.form-group -->
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kompetensi Dasar</label>
                                <textarea name="kd" class="form-control rounded-0" rows="3" placeholder="Enter ...">{{$item->kd}}</textarea>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Materi Pokok</label>
                                <textarea name="materi" class="form-control rounded-0" rows="3" placeholder="Enter ...">{{$item->materi}}</textarea>
                            </div>
                            <!-- /.form-group -->
                        </div>
                    </div>
                    <!-- /.row -->
                  
            </div>
            <div class="modal-footer justify-content-right">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    