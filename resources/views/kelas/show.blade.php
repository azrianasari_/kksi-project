@extends('layouts.main')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          {{-- <h1>Buttons</h1> --}}
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Daftar Jadwal</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
        
          <form action="{{route('kelas.store')}}" method="POST">
            @csrf
            <div class="card card-default">
                <div class="card-header">
                <h3 class="card-title">Buka Kelas {{$jadwal->mapel->nama}} / <strong>{{$jadwal->kelas_formatted}}</strong></h3>
      
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                        <label>Tanggal</label>
                        <div class="form-group">
                              <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                  <input type="date" name="tanggal" class="form-control datetimepicker-input rounded-0" data-target="#reservationdate">
                                  <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                      <div class="input-group-text rounded-0"><i class="fa fa-calendar"></i></div>
                                  </div>
                              </div>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Jadwal</label>
                        <input type="hidden" name="jadwal_id" value="{{$jadwal->id}}">
                        <input type="text" class="form-control rounded-0" id="exampleInputEmail1" placeholder="Enter email" value="{{$jadwal->hari_formatted}} / {{$jadwal->waktu_awal}} - {{$jadwal->waktu_akhir}} ">
                      </div>
                      <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kompetensi Dasar</label>
                            <textarea name="kd" class="form-control rounded-0" rows="3" placeholder="Enter ..."></textarea>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Materi Pokok</label>
                            <textarea name="materi" class="form-control rounded-0" rows="3" placeholder="Enter ..."></textarea>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
      
            
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary rounded-0 .tombol-simpan">Buka Kelas</button>
                </div>
              </div>
            </form>
              <!-- /.card -->
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <b>Hallo</b> {{$jadwal->pegawai->nama}}
              </h3>
            </div>


             <!-- /.card-header -->
             <div class="card-body">
               <div class="row">
                  <div class="col-lg-10">
                      <p>Daftar <code>Pertemuan</code></p>
                  </div>
                  <div class="col-lg-2">
                      <div class="card-tools">
                          <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
          
                            <div class="input-group-append">
                              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                            </div>      
                          </div>
                        </div>
                  </div>

               </div>
  
              @include('layouts.flash') 
              <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Tanggal / Buka Kelas</th>
                    <th>Kelas</th>
                    <th>Kompetensi Dasar</th>
                    <th>Materi Pokok</th>
                    <th>Bukti</th>
                    <th>Absensi</th>
                    <th style="width: 60px"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data as $item)
                  <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$item->hari_formatted}}, {{date('d M y',strtotime($item->tanggal)) }} / {{$item->waktu}}</td>
                      <td>{{$item->jadwal->kelas_formatted}}</td>
                      <td>{{$item->kd}}</td>
                      <td>{{$item->materi}}</td>
                      <td><a href="#" data-toggle="modal" data-target="#modal-lg-upload-{{$item->id}}" class="btn btn-block btn-warning btn-sm btn-flat text-white"><i class="fas fa-upload"></i></a></td>
                      <td>
                        <div class="row">
                          <div class="col-lg-6">
                              <a href="{{route('absensi.cetak',$item->id)}}" target="_blank"  class="btn btn-block btn-success btn-sm btn-flat text-white"><i class="fas fa-print"></i></a>
                          </div>
                          <div class="col-lg-6">
                              <a href="{{route('absensi.show',$item->id)}}" class="btn btn-block btn-info btn-sm btn-flat text-white"><i class="fas fa-list"></i></a>
                          </div>
                        </div>
                      </td>
                      <td>
                          <div class="btn-group">
                              <button type="button" class="btn btn-danger btn-sm btn-flat">Action</button>
                              <button type="button" class="btn btn-danger btn-sm btn-flat dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                              </button>
                                {{-- <div class="dropdown-menu" role="menu" style="">
                                  <a class="dropdown-item" href="#">Edit</a>
                                  <a class="dropdown-item" href="#">Delete</a>
                                </div> --}}
                             
                              <div class="dropdown-menu" role="menu" style="">
                                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-lg-{{$item->id}}">Edit</a>
                                  <form action={{"/kelas/$item->id"}} method="POST" class="d-inline">
                                    @method('delete')
                                    @csrf
                                      <input type="hidden" name="jadwal_id" value="{{$jadwal->id}}">
                                      <button type="submit" class="dropdown-item" onclick="return confirm('Yakin?')">Hapus</button>
                                  </form>
                                </div>
                            </div>
                      </td>
                    </tr>  
                    @include('kelas.edit', [$item])
                    @include('kelas.upload', [$item])
              
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                  {{ $data->links() }}
              </ul>
            </div>
            
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- ./row -->

    
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>


@endsection