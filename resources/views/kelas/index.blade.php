@extends('layouts.main')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                {{-- <h1>Widgets</h1> --}}
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Kelas</li>
                </ol>
              </div>
            </div>
            @if(auth()->user()->pegawai->level == 1)
            <div class="row">
                <div class="col-12">
                  <div class="callout callout-info">
                    {{-- <h5><i class="fas fa-search"></i> Cari :</h5> --}}

                    <div class="row">
                      <div class="col-lg-3">
                          <div class="form-group">
                              <label for="hari">Hari</label>
                              <select id="hari" name="hari" class="form-control select2 rounded-0" style="width: 100%;" required>
                                  <option value = "">-- pilih --</option>
                                  <option value = "1">Senin</option>
                                  <option value = "2">Selasa</option>
                                  <option value = "3">Rabu</option>
                                  <option value = "4">Kamis</option>
                                  <option value = "5">Jumat</option>
                                </select>
                            </div>  
                      </div>
                      <div class="col-lg-3">
                          <div class="form-group">
                              <label for="kelas">Kelas</label>
                              <select id="kelas" name="kelas" class="form-control select2 rounded-0" style="width: 100%;" required>
                                <option value = "">-- pilih --</option>
                                <option value = "1">X RPL</option>
                                <option value = "2">X TKJ</option>
                                <option value = "3">X MM</option>
                                <option value = "4">XI RPL</option>
                                <option value = "5">XI TKJ</option>
                                <option value = "6">XI MM</option>
                                <option value = "7">XII RPL</option>
                                <option value = "8">XII TKJ</option>
                                <option value = "9">XII MM</option>
                              </select>
                            </div>  
                      </div>
                      <div class="col-lg-3">
                          <div class="form-group">
                              <label for="mapel_id">Mata Pelajaran</label>
                              <select id="mapel_id" name="mapel_id" class="form-control select2 rounded-0" style="width: 100%;" required>
                                <option value="">-- pilih --</option>
                                @foreach ($mapel as $m)
                                  <option value="{{$m->id}}"> {{ $m->nama }}</option>
                                @endforeach
                              </select>
                            </div>  
                      </div>
                      <div class="col-lg-3">
                          <div class="form-group">
                              <label for="pegawai_id">Guru</label>
                              <select id="pegawai_id" name="pegawai_id" class="form-control select2 rounded-0" style="width: 100%;" required>
                                <option value="">-- pilih --</option>
                                  @foreach ($pegawai as $p)
                                      <option value="{{$p->id}}"> {{ $p->nama }}</option>
                                  @endforeach
                              </select>
                            </div>  
                      </div>
                    </div>
                    {{-- This page has been enhanced for printing. Click the print button at the bottom of the invoice to test. --}}
                    <div class="text-right">
                      <button type="button" id="btn-cari" class="btn btn-primary rounded-0"><i class="fas fa-search"></i> Cari</button>
                    </div>
                  </div>
                </div><!-- /.col -->
              </div>
              @endif
          </div><!-- /.container-fluid -->
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <h5 class="mt-2 mb-2">Daftar Kelas</h5>
            <div class="row"v id="list-kelas">
              @include('kelas.list_kelas')             
            </div>
          </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    
        <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
          <i class="fas fa-chevron-up"></i>
        </a>
      </div>
      <!-- /.content-wrapper -->
    
@endsection
