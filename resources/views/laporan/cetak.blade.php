<!DOCTYPE html>
<html>
<head>
<title>Absensi {{$jadwal->mapel->nama}}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Kehadiran Siswa pada KBM</h4>
		<h6>Kelas {{$jadwal->kelas_formatted}}</h5>
    </center>
    <table>
        <tr>
            <th>Mata Pelajaran</th>
            <th style="padding-left:10px">:</th>
            <th style="padding-left:10px">{{$jadwal->mapel->nama}}</th>
        </tr>
        <tr>
            <th>Jumlah Pertemuan</th>
            <th style="padding-left:10px">:</th>
            <th style="padding-left:10px">{{$jadwal->daftar_kelas->count()}}</th>
      
        </tr>
        <tr>
            <th>Guru</th>
            <th style="padding-left:10px">:</th>
            <th style="padding-left:10px">{{$jadwal->pegawai->nama}}</th>
      
        </tr>
    </table>
  
	<table class='table table-bordered mt-2'>
		<thead>
			<tr>
                <th class="text-center" style="width: 10px" rowspan="2">#</th>
                <th class="text-center" rowspan="2">NISN / NIS</th>
                <th class="text-center" rowspan="2">Nama</th>
                <th class="text-center" colspan="{{$jadwal->daftar_kelas->count()}}">Pertemuan ke-</th>
            </tr>
            <tr>
                @foreach ($jadwal->daftar_kelas as $pertemuan)
                <th class="text-center" >{{$loop->iteration}}</th>    
                @endforeach
            </tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
            @foreach ($data as $item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->nisn}} / {{$item->nis}}</td>
                <td>{{$item->nama}}</td>
                @foreach ($jadwal->daftar_kelas as $pertemuan)
                    @foreach ($item->absensi->where('daftar_kelas_id', $pertemuan->id) as $absen)
                        <td>{{$absen->keterangan_formatted}}</td>
                    @endforeach
                @endforeach
                
            </tr>
         
            @endforeach
		</tbody>
	</table>
 
</body>
</html>