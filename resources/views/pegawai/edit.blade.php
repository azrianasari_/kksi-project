@extends('layouts.main')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            {{-- <h1>Validation</h1> --}}
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data <small>Pegawai</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form role="form" id="quickForm" method="POST" action="{{ "/pegawai/$data->id" }}">
              @method('put')
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nisn">NIP</label>
                    <input type="text" name="nip" class="form-control rounded-0" id="nip" placeholder=" Enter NIP" required value="{{$data->nip}}">
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control rounded-0" id="nama" placeholder=" Enter Nama" required value="{{$data->nama}}">
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" name="alamat" class="form-control rounded-0" id="alamat" placeholder=" Enter Alamat" required value="{{$data->alamat}}">
                  </div>
                  <div class="form-group">
                    <label for="no_telp">Nomor Telepon</label>
                    <input type="text" name="no_telp" class="form-control rounded-0" id="no_telp" placeholder=" Enter Nomor Telepon" required value="{{$data->no_telp}}">
                  </div>
                  <div class="form-group">
                      <div class="form-group">
                          <label for="level">Jabatan</label>
                          <select class="form-control select2 rounded-0" style="width: 100%;" id="level" name="level" required>
                            <option value = "">-- pilih --</option>
                            <option value = "0" @if($data->level == "0") selected @endif >Guru</option>
                            <option value = "1" @if($data->level == "1") selected @endif >Admin</option>
                          </select>
                        </div>   
                    </div>
                   
                  {{-- <div class="form-group mb-0">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                      <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                    </div>
                  </div> --}}
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-right">
                  <button type="submit" class="btn btn-primary rounded-0">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
    
@endsection