<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    {{-- <a href="index3.html" class="brand-link">
      <img src="{{asset('assets/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a> --}}

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('assets/dist/img/logo.jpeg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{auth()->user()->pegawai->nama}}</a>
        </div>
      </div>
   
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
            <a href="{{url('dashboard')}}" class="nav-link {{Request::is('dashboard')?'active':''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @if(auth()->user()->pegawai->level == 1)
       
          <li class="nav-item">
              <a href="{{route('siswa.index')}}" class="nav-link {{Request::is('siswa')?'active':''}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Siswa
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('pegawai.index')}}" class="nav-link {{Request::is('pegawai')?'active':''}}">
              <i class="nav-icon far fa-address-card"></i>
              <p>
                Pegawai
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('mapel.index')}}" class="nav-link {{Request::is('mapel')?'active':''}}">
              <i class="nav-icon fas fa-book-open"></i>
              <p>
                Mata Pelajaran
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('jadwal.index')}}" class="nav-link {{Request::is('jadwal')?'active':''}}">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Jadwal
              </p>
            </a>
          </li>          
          
          <li class="nav-header">ABSENSI</li>
          <li class="nav-item has-treeview">
              <a href="{{route('kelas.index')}}" class="nav-link {{Request::is('kelas')?'active':''}}">
                <i class="nav-icon fab fa-buromobelexperte"></i>
                <p>
                  Buka Kelas
                </p>
              </a>
          </li>
        
          <li class="nav-item has-treeview">
              <a href="{{route('laporan.index')}}" class="nav-link {{Request::is('laporan')?'active':''}}">
                <i class="nav-icon fas fa-file-medical-alt"></i>
                <p>
                  Laporan
                </p>
              </a>
          </li>
          @endif

          @if(auth()->user()->pegawai->level == 0)
          <li class="nav-header">ABSENSI</li>
          <li class="nav-item has-treeview">
              <a href="{{route('kelas.pegawai')}}" class="nav-link {{Request::is('daftar/kelas')?'active':''}}">
                <i class="nav-icon fab fa-buromobelexperte"></i>
                <p>
                  Buka Kelas
                </p>
              </a>
          </li>
        
          <li class="nav-item has-treeview">
              <a href="{{route('laporan.pegawai')}}" class="nav-link {{Request::is('daftar/laporan')?'active':''}}">
                <i class="nav-icon fas fa-file-medical-alt"></i>
                <p>
                  Laporan
                </p>
              </a>
          </li>
          @endif
          


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>