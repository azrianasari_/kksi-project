@if ($message = Session::get('success'))
<div class="alert alert-success rounded-0">
  <button type="button" class="close" data-dismiss="alert">×</button> 
  <i class="fas fa-check"></i> &nbsp;<span>{{ $message }}</span>
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger rounded-0">
  <button type="button" class="close" data-dismiss="alert">×</button> 
  <i class="fas fa-times"></i> &nbsp;<span>{{ $message }}</span>
</div>
@endif