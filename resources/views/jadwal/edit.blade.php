@extends('layouts.main')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            {{-- <h1>Validation</h1> --}}
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data <small>Jadwal</small></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form role="form" id="quickForm" method="POST" action="{{ "/jadwal/$data->id" }}">
              @method('put')
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <select id="kelas" name="kelas" class="form-control select2 rounded-0" style="width: 100%;" required>
                          <option value = "">-- pilih --</option>
                          <option value = "1" @if($data->kelas == '1') selected @endif>X RPL</option>
                          <option value = "2" @if($data->kelas == '2') selected @endif>X TKJ</option>
                          <option value = "3" @if($data->kelas == '3') selected @endif>X MM</option>
                          <option value = "4" @if($data->kelas == '4') selected @endif>XI RPL</option>
                          <option value = "5" @if($data->kelas == '5') selected @endif>XI TKJ</option>
                          <option value = "6" @if($data->kelas == '6') selected @endif>XI MM</option>
                          <option value = "7" @if($data->kelas == '7') selected @endif>XII RPL</option>
                          <option value = "8" @if($data->kelas == '8') selected @endif>XII TKJ</option>
                          <option value = "9" @if($data->kelas == '9') selected @endif>XII MM</option>
                        </select>
                      </div>   
                  </div>
                  <div class="form-group">
                    <div class="form-group">
                        <label for="hari">Hari</label>
                        <select id="hari" name="hari" class="form-control select2 rounded-0" style="width: 100%;" required>
                          <option value = "">-- pilih --</option>
                          <option value = "1" @if($data->hari == '1') selected @endif >Senin</option>
                          <option value = "2" @if($data->hari == '2') selected @endif >Selasa</option>
                          <option value = "3" @if($data->hari == '3') selected @endif >Rabu</option>
                          <option value = "4" @if($data->hari == '4') selected @endif >Kamis</option>
                          <option value = "5" @if($data->hari == '5') selected @endif >Jumat</option>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <div class="form-group">
                        <label for="mapel_id">Mata Pelajaran</label>
                        <select id="mapel_id" name="mapel_id" class="form-control select2 rounded-0" style="width: 100%;" required>
                          <option selected="selected">-- pilih --</option>
                          @foreach ($mapel as $m)
                            <option value="{{$m->id}}" @if($data->mapel_id == $m->id) selected @endif > {{ $m->nama }}</option>
                          @endforeach
                        </select>
                      </div>   
                  </div>
                  <div class="form-group">
                    <div class="form-group">
                        <label for="pegawai_id">Guru</label>
                        <select id="pegawai_id" name="pegawai_id" class="form-control select2 rounded-0" style="width: 100%;" required>
                          <option value="">-- pilih --</option>
                            @foreach ($pegawai as $p)
                                <option value="{{$p->id}}" @if($data->pegawai_id == $p->id) selected @endif> {{ $p->nama }}</option>
                            @endforeach
                        </select>
                      </div>   
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                        <label for="waktu_awal">Jam Mulai</label>
                        <input type="time" name="waktu_awal" class="form-control rounded-0" id="waktu_awal" value="{{$data->waktu_awal}}">             
                    </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        <label for="waktu_akhir">Jam Selesai</label>
                        <input type="time" name="waktu_akhir" class="form-control rounded-0" id="waktu_akhir" value="{{$data->waktu_akhir}}">    
                        </div>
                    </div>
                  </div>
               
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-right">
                  <button type="submit" class="btn btn-primary rounded-0">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
    
@endsection