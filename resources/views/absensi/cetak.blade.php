<!DOCTYPE html>
<html>
<head>
<title>Absensi {{$kelas->jadwal->mapel->nama}}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Kehadiran Siswa pada KBM</h4>
		<h6>Kelas {{$kelas->jadwal->kelas_formatted}}</h5>
    </center>
    <table>
        <tr>
            <th>Mapel</th>
            <th style="padding-left:10px">:</th>
            <th style="padding-left:10px">{{$kelas->jadwal->mapel->nama}}</th>
        </tr>
        <tr>
            <th>Kompetensi Dasar</th>
            <th style="padding-left:10px">:</th>
            <th style="padding-left:10px">{{$kelas->kd}}</th>
      
        </tr>
        <tr>
            <th>Materi Pokok</th>
            <th style="padding-left:10px">:</th>
            <th style="padding-left:10px">{{$kelas->materi}}</th>
      
        </tr>
    </table>
  
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th style="width: 10px">#</th>
                <th>NISN / NIS</th>
                <th>Nama</th>
                <th>Keterangan</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
            @foreach ($data as $item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <input type="hidden" name="id[]" value="{{$item->id}}">
                <input type="hidden" name="siswa_id[]" value="{{$item->siswa_id}}">
                <td>{{$item->siswa->nisn}} / {{$item->siswa->nis}}</td>
                <td>{{$item->siswa->nama}}</td>
                <td>{{$item->keterangan_formatted}}</td>
              
              </tr>
         
            @endforeach
		</tbody>
	</table>
 
</body>
</html>