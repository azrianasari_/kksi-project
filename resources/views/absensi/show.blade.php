@extends('layouts.main')
@section('content')

<style>
  .form-group {
    margin-bottom: 0!important;
  }
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          {{-- <h1>Buttons</h1> --}}
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Daftar Siswa</li>
          </ol>
        </div>
      </div>
      
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          {{-- form --}}
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                Absensi Siswa {{$kelas->jadwal->kelas_formatted}}
              </h3>
            </div>
            
             <!-- /.card-header -->
             <div class="card-body">
               <div class="row mb-2">
                  <div class="col-lg-10">
                      <p>Mata Pelajaran : <code>{{$kelas->jadwal->mapel->nama}}</code></p>
                  </div>
                  <div class="col-lg-2">
                      <div class="card-tools">
                          <div class="input-group input-group-sm" >
                            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
          
                            <div class="input-group-append">
                              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                            </div>      
                          </div>
                        </div>
                  </div>

               </div>

          <form action="{{route('absensi.update',$kelas->id)}}" method="POST" >
              @method('put')
              @csrf
             
              <input type="hidden" name="daftar_kelas_id" value="{{$kelas->id}}">
              
              @include('layouts.flash') 
             
              <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>NISN / NIS</th>
                    <th>Nama</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($data as $item)
                  <tr>
                      <td>{{$loop->iteration}}</td>
                      <input type="hidden" name="id[]" value="{{$item->id}}">
                      <input type="hidden" name="siswa_id[]" value="{{$item->siswa_id}}">
                      <td>{{$item->siswa->nisn}} / {{$item->siswa->nis}}</td>
                      <td>{{$item->siswa->nama}}</td>
                      <td>
                        <div class="form-group">
                          <select class="form-control select2 rounded-0" style="width: 100%;" name="keterangan[]" required>
                            <option value = "1" @if($item->keterangan == 1 ) selected @endif>Hadir</option>
                            <option value = "2" @if($item->keterangan == 2 ) selected @endif>Izin</option>
                            <option value = "3" @if($item->keterangan == 3 ) selected @endif>Sakit</option>
                            <option value = "4" @if($item->keterangan == 4 ) selected @endif>Alpa</option>
                          </select>
                        </div>
                      </td>
                    
                    </tr>
               
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix text-right">
              <button type="submit" class="btn btn-primary rounded-0">Submit</button>
            </div>
            
          </div>
          </form>
          {{-- /form --}}
        </div>
        <!-- /.col -->
      </div>
      <!-- ./row -->

    
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>



@endsection