<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kelas;
use App\Jadwal;
use App\Absensi;
use App\Mapel;
use App\Pegawai;
use App\Siswa;
use PDF;

class AbsensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     
        $data = Absensi::where('daftar_kelas_id',$id)->get();
       
        $kelas = Kelas::findOrFail($id);
        
        return view('absensi.show',compact('data','kelas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        foreach($request->id as $key => $sw) {
            Absensi::where('id', $sw)
            ->update([
                'daftar_kelas_id' => $id,
                'siswa_id' => $request->siswa_id[$key],
                'keterangan' => $request->keterangan[$key]
            ]);
        }

         //arahkan ke halaman absensi
         return redirect(route('absensi.show', $id))->with(['success' => 'Berhasil simpan data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cetak($id)
    {
        $kelas = Kelas::findOrFail($id);
        $data = Absensi::where('daftar_kelas_id',$id)->get();
 
    	$pdf = PDF::loadview('absensi.cetak',compact('data','kelas'));
    	return $pdf->stream();
    }

    //menu laporan
    public function laporan()
    {
       //tampilkan data mapel
       $mapel = Mapel::all();
    
       //tampilkan data pagawai
       $pegawai = Pegawai::all();

       $data = Jadwal::get();
       return view('laporan.index',compact('data','mapel','pegawai'));
    }

    public function getKelas(Request $request)
    {
        $data     = Jadwal::filter($request)->get();
        
        $response   = [
            'status'            => false,
            'message'           => 'Data tidak ditemukan',
            'data'              => null
        ];

        if ($data->isNotEmpty()) {
            $response   = [
                'status'            => true,
                'message'           => 'Data ditemukan',
                'data'              => view('laporan.list_kelas', compact('data'))->render(),
            ];
        }

        return response()->json($response);
    }

    public function cetakRekap($id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $data = Siswa::where('kelas',$jadwal->kelas)->get();
  
        $pdf = PDF::setPaper('legal', 'landscape')
        ->loadview('laporan.cetak',compact('data','jadwal'));
    	return $pdf->stream();
    }


    //menu laporan
    public function laporanPegawai()
    {
        $data = Jadwal::where('pegawai_id', auth()->user()->pegawai->id)->get();
        
        return view('laporan.index',compact('data'));
    }

}
