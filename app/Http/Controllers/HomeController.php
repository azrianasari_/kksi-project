<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pegawai = \App\Pegawai::count();
        $siswa = \App\Siswa::count();
        $jadwal = \App\Jadwal::count();  
        $absensi = \App\Absensi::count();
        
        $hadir = \App\Absensi::where('keterangan', '1')->count();
        $izin = \App\Absensi::where('keterangan', '2')->count();
        $sakit = \App\Absensi::where('keterangan', '3')->count();
        $alpa = \App\Absensi::where('keterangan', '4')->count();

        $total = $hadir + $izin + $sakit + $alpa;
        
        return view('dashboard.index', compact('pegawai','siswa','jadwal','absensi',
                                                'hadir','izin','sakit','alpa','total'));
    }
}
