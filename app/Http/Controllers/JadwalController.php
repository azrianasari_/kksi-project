<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal;
use App\Mapel;
use App\Pegawai;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Jadwal::orderBy('id','DESC')->paginate(10);
        return view('jadwal.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //tampilkan data mapel
        $mapel = Mapel::all();
        
        //tampilkan data pagawai
        $pegawai = Pegawai::all();

        return view('jadwal.create',compact('mapel','pegawai'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Jadwal::create([
            'kelas' => $request->kelas,
            'hari' => $request->hari,
            'waktu_awal' => $request->waktu_awal,
            'waktu_akhir' => $request->waktu_akhir,
            'mapel_id' => $request->mapel_id,
            'pegawai_id' => $request->pegawai_id
        ]);

        //arahkan ke halaman siswa
        return redirect('jadwal')->with(['success' => 'Berhasil simpan data']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //tampilkan data mapel
        $mapel = Mapel::all();
    
        //tampilkan data pagawai
        $pegawai = Pegawai::all();
  
        //tampilkan data berdasarkan $id data
        $data = Jadwal::findOrFail($id);
        //arahkan dan tampilkan data pada halaman edit
        return view('jadwal.edit', compact('data','mapel','pegawai'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->update([
            'kelas' => $request->kelas,
            'hari' => $request->hari,
            'waktu_awal' => $request->waktu_awal,
            'waktu_akhir' => $request->waktu_akhir,
            'mapel_id' => $request->mapel_id,
            'pegawai_id' => $request->pegawai_id
       
            ]);
        
        return redirect('jadwal')->with(['success' => 'Berhasil ubah data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->destroy($id);

        //arahkan ke halaman index
        return redirect('jadwal')->with(['success' => 'Berhasil hapus data']);
    }
}
