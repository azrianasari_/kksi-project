<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Jadwal;

class JadwalController extends Controller
{
    public function index()
    {
      
        $jadwal = Jadwal::where('kelas',Auth::user()->siswa->kelas)->get()->map(function($item){
            return [
                'id'=> $item->id,
                'kelas'=> $item->kelas_formatted,
                'hari'=> $item->hari_formatted,
                'waktu_awal'=> $item->waktu_awal ,
                'waktu_akhir'=> $item->waktu_akhir ,
                'mapel_id'=> $item->mapel->nama ,
                'pegawai_id'=> $item->pegawai->nama ,
                'created_at'=> $item->created_at ,
                'updated_at'=>$item->updated_at 
            ];
        });

        return response()->json([
            'success' => true,
            'message' => 'List of All Data',
            'data' => $jadwal
        ], 200);
    }
}
