<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Absensi;
use DB;

class AbsensiController extends Controller
{
    public function show($jadwal) 
    {
        $absen = Absensi::with('daftar_kelas')->where('siswa_id', Auth::user()->siswa->id)
                    ->whereHas('daftar_kelas',function ($query) use ($jadwal){
                        $query->where('jadwal_id', $jadwal);
                    })->get()->last();

        return response()->json([
            'success' => true,
            'message' => 'List of All Data',
            'data' => $absen
        ], 200);

    }

    public function absenSiswa(Request $request, $id) 
    {    
        try {

            DB::beginTransaction();

            $success  = Absensi::where('id', $id)
                         ->update([
                             'keterangan' => $request->keterangan,
                             'status' => '1'
                         ]);
            DB::commit();

            return response()->json([
                'status' => $success,
                'data' => ['keterangan' => $request->keterangan],
                'message' => 'Berhasil Absensi'
            ], 200);

        } catch(\Exception $e) {
            DB::rollBack();

            throw $e;
        }

    }
}
