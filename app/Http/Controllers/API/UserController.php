<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    public function login(){
      
        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
            if(Auth::user()->ket == "siswa"){
                $user = Auth::user();
                $success['token'] =  $user->createToken('nApp')->accessToken;
                $data = [
                    'nis'=> Auth::user()->siswa->nis,
                ];
                return response()->json([
                    'success' => $success,
                    'data' => $data
                ], $this->successStatus);
            }     
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}
