<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal;
use App\Kelas;
use App\Siswa;
use App\Absensi;
use App\Mapel;
use App\Pegawai;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //tampilkan data mapel
        $mapel = Mapel::all();
    
        //tampilkan data pagawai
        $pegawai = Pegawai::all();

        $data = Jadwal::get();
        return view('kelas.index',compact('data','mapel','pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $daftar_kelas = Kelas::create([
                            'tanggal' => $request->tanggal,
                            'waktu' =>  \Carbon\Carbon::now('Asia/Makassar'),
                            'kd' => $request->kd,
                            'materi' => $request->materi,
                            'jadwal_id' => $request->jadwal_id,
                            'pegawai_id' => auth()->user()->pegawai->id
                        ]);

        //dapatkan data kelas dari tabel jadwal
        $kelas = Jadwal::findOrFail( $request->jadwal_id)->kelas;
        
        //dapatkan data siswa berdasarkan kelas
        $siswa = Siswa::where('kelas', $kelas)->get();


        foreach($siswa as $sw) {
            Absensi::create([
                'daftar_kelas_id' => $daftar_kelas->id,
                'siswa_id' => $sw->id,
                'keterangan' => '4'
            ]);
        }
       

        //arahkan ke halaman kelas
        return redirect(route('kelas.show', $request->jadwal_id))->with(['success' => 'Berhasil simpan data']);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jadwal = Jadwal::findOrFail($id);

        $data = Kelas::where('jadwal_id',$id)->orderBy('id','DESC')->paginate(10);
       
        return view('kelas.show',compact('data','jadwal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kelas = Kelas::findOrFail($id);
        $kelas->update([
            'tanggal' => $request->tanggal,
            'waktu' =>  \Carbon\Carbon::now('Asia/Makassar'),
            'kd' => $request->kd,
            'materi' => $request->materi,
            'jadwal_id' => $request->jadwal_id,
            'pegawai_id' => auth()->user()->pegawai->id
       ]);
        
        return redirect(route('kelas.show', $request->jadwal_id))->with(['success' => 'Berhasil ubah data']);
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $kelas = Kelas::findOrFail($id);
        $kelas->destroy($id);

        //arahkan ke halaman kelas
        return redirect(route('kelas.show', $request->jadwal_id))->with(['success' => 'Berhasil hapus data']);
    }

    public function uploadImage(Request $request, $id)
    {

       $file = $request->file('foto');
       $name = \Carbon\Carbon::now()->format('Y-m-dH:i:s').'-' .$file->getClientOriginalName(). '.' . $file->getClientOriginalExtension();
       $path = $file->storeAs('/public/images', $name);
       $kelas = Kelas::findOrFail($id);
       $kelas->update(['foto' => $name ]);
       return redirect(route('kelas.show', $request->jadwal_id))->with(['success' => 'Berhasil upload bukti mengajar']);
 
    }

    public function kelasPegawai()
    {
        $data = Jadwal::where('pegawai_id', auth()->user()->pegawai->id)->get();
        
        return view('kelas.index',compact('data'));
    }

    
    
}
