<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\User;
use Illuminate\Support\Facades\Hash;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user()->pegawai->nip;
        $data = Pegawai::where('nip','!=',$user)->orderBy('id','DESC')->paginate(10);
        return view('pegawai.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pegawai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        Pegawai::create([
            'nip'=> $request->nip,
            'nama'=> $request->nama,
            'alamat'=> $request->alamat,
            'no_telp'=> $request->no_telp,
            'level'=> $request->level
        ]);

        User::create([
            'username'=> $request->nip,
            'password'=> Hash::make('123123'),
            'ket'=> 'pegawai'
        ]);

        //arahkan ke halaman pegawai
        return redirect('pegawai')->with(['success' => 'Berhasil simpan data']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          //tampilkan data berdasarkan $id data
          $data = Pegawai::findOrFail($id);
          //arahkan dan tampilkan data pada halaman edit
          return view('pegawai.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pegawai = Pegawai::findOrFail($id);
        $pegawai->update([
                'nip'=> $request->nip,
                'nama'=> $request->nama,
                'alamat'=> $request->alamat,
                'no_telp'=> $request->no_telp,
                'level'=> $request->level
            ]);
        
        User::where('username', $pegawai->nip )
            ->update([
            'username' => $request->nip,
        ]);

        return redirect('pegawai')->with(['success' => 'Berhasil ubah data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = Pegawai::findOrFail($id);

        User::where('username', $pegawai->nip)->delete();
        //hapus data berdasarkan $id
        $pegawai->destroy($id);

        //arahkan ke halaman pegawai
        return redirect('pegawai')->with(['success' => 'Berhasil hapus data']);
    }

    public function reset($id)
    {
        $pegawai = Pegawai::findOrFail($id);

        User::where('username', $pegawai->nis )
            ->update([
            'password' => Hash::make('123123'),
        ]);

        //arahkan ke halaman pegawai
        return redirect('pegawai')->with(['success' => 'Berhasil reset password']);  
    }
}
