<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'daftar_kelas';

    protected $fillable = ['tanggal','waktu', 'kd', 'materi', 'foto', 'jadwal_id', 'pegawai_id'];

    //relasi ke tabel jadwal
    public function jadwal()
    {
        return $this->belongsTo('App\Jadwal');
    }

     //relasi ke tabel pegawai
     public function pegawai()
     {
         return $this->belongsTo('App\Pegawai');
     }

     //relasi ke tabel absen
     public function absensi(){
    	return $this->hasMany('App\Absensi','daftar_kelas_id');
    }

    public function getHariFormattedAttribute()
    {
        $value = '';

        $tanggal = date('D',strtotime($this->tanggal));
      
        if($tanggal == 'Mon' ){
            $value = "Senin";
        }
        if($tanggal == 'Tue' ){
            $value = "Selasa";
        }
        if($tanggal == 'Wed' ){
            $value = "Rabu";
        }
        if($tanggal == 'Thu' ){
            $value = "Kamis";
        }
        if($tanggal == 'Fri'){
            $value = "Jumat";
        }
        if($tanggal == 'Sat'){
            $value = "Sabtu";
        }
        if($tanggal == 'Sun'){
            $value = "Minggu";
        }

        return $value;
    }



}
