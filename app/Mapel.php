<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $table = 'mapel';

    protected $fillable = ['kode','nama'];

     //relasi ke tabel mapel, 1 pegawai dapat memiliki banyak mapel
     public function mapel(){
    	return $this->hasMany('App\Mapel');
    }
}
