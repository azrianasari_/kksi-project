<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';

    protected $fillable = ['nisn','nis','nama','alamat','no_telp','kelas'];

    
    public function absensi(){
        return $this->hasMany('App\Absensi','siswa_id');
    }

}
