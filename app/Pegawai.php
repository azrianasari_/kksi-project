<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';

    protected $fillable = ['nip','nama','alamat','no_telp','level'];

    //relasi ke tabel jadwal, 1 pegawai dapat memiliki banyak jadwal
    public function jadwal(){
    	return $this->hasMany('App\Jadwal');
    }

    public function users()
     {
     	return $this->belongsTo('App\User');
     }

}
