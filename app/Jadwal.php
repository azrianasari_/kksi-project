<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';

    protected $fillable = ['kelas', 'hari', 'waktu_awal','waktu_akhir', 'mapel_id', 'pegawai_id'];

     //relasi ke tabel mapel
     public function mapel()
     {
     	return $this->belongsTo('App\Mapel');
     }

      //relasi ke tabel pegawai
      public function pegawai()
      {
          return $this->belongsTo('App\Pegawai');
      }

      public function daftar_kelas(){
      	return $this->hasMany('App\Kelas','jadwal_id');
      }

      public function getHariFormattedAttribute()
        {
            $value = '';

            if($this->hari == 1 ){
                $value = "Senin";
            }
            if($this->hari == 2 ){
                $value = "Selasa";
            }
            if($this->hari == 3 ){
                $value = "Rabu";
            }
            if($this->hari == 4 ){
                $value = "Kamis";
            }
            if($this->hari == 5){
                $value = "Jumat";
            }

            return $value;
        }

        public function getKelasFormattedAttribute()
        {
            $value = '';

            if($this->kelas == 1 ){
                $value = "X RPL";
            }
            if($this->kelas == 2 ){
                $value = "X TKJ";
            }
            if($this->kelas == 3 ){
                $value = "X MM";
            }
            if($this->kelas == 4 ){
                $value = "XI RPL";
            }
            if($this->kelas == 5 ){
                $value = "XI TKJ";
            }
            if($this->kelas == 6 ){
                $value = "XI MM";
            }
            if($this->kelas == 7 ){
                $value = "XII RPL";
            }
            if($this->kelas == 8 ){
                $value = "XII TKJ";
            }
            if($this->kelas == 9 ){
                $value = "XII MM";
            }
           
            return $value;
        }
    

        public function scopeFilter($query, $request)
        {
            if ($request->has('hari') && $request->hari != null) {
                $query->where('hari', $request->hari);
            }

            if ($request->has('kelas') && $request->kelas != null) {
                $query->where('kelas', $request->kelas);
            }

            if ($request->has('mapel_id') && $request->mapel_id != null) {
                $query->where('mapel_id', $request->mapel_id);
            }

            if ($request->has('pegawai_id') && $request->pegawai_id != null) {
                $query->where('pegawai_id', $request->pegawai_id);
            }

            return $query;
        }

}
