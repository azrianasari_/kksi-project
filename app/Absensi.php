<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $table = 'absensi';

    protected $fillable = ['daftar_kelas_id', 'siswa_id','keterangan'];

     //relasi ke tabel jadwal
     public function siswa()
     {
         return $this->belongsTo('App\Siswa');
     }

     //relasi ke tabel daftar kelas
     public function daftar_kelas()
     {
         return $this->belongsTo('App\Kelas');
     }

     public function getKeteranganFormattedAttribute()
     {
         $value = '';

         if($this->keterangan == 1 ){
             $value = "Hadir";
         }
         if($this->keterangan == 2 ){
             $value = "Izin";
         }
         if($this->keterangan == 3 ){
             $value = "Sakit";
         }
         if($this->keterangan == 4 ){
             $value = "Alpa";
         }

         return $value;
     }

}
