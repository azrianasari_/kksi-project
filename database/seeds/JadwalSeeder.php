<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Mapel;
use App\Pegawai;


class JadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //'kelas', 'hari', 'waktu_awal','waktu_akhir', 'mapel_id', 'pegawai_id'

        $faker = Faker::create('id');
        $mapel = Mapel::get()->pluck('id');
        $pegawai = Pegawai::get()->pluck('id');

        for($i = 1; $i <= 20; $i++){
 
            // insert data ke table pegawai menggunakan Faker
          DB::table('jadwal')->insert([
            'kelas' => $faker->randomElement($array = array ('1','2','3'), $count = 1),
            'hari' => $faker->randomElement($array = array ('1','2','3','4','5'), $count = 1),
            'waktu_awal' => $faker->time($format = 'H:i:s', $max = 'now'),
            'waktu_akhir' => $faker->time($format = 'H:i:s', $max = 'now'),
            'mapel_id' => $faker->randomElement($array = $mapel, $count = 1),
            'pegawai_id' => $faker->randomElement($array = $pegawai, $count = 1),
            'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
            'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
             
          ]);
        }
    }
}
