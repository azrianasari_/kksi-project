<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Siswa;     
use App\User;     

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id');
        for($i = 1; $i <= 50; $i++){
 
            // insert data ke table pegawai menggunakan Faker
        $siswa = Siswa::create([
            'nisn' => $faker->numberBetween($min = 1000, $max = 9000),
            'nis' => $faker->numberBetween($min = 1000, $max = 9000),
            'nama' => $faker->name,
            'alamat' => $faker->address,
            'no_telp' => $faker->phoneNumber,
            'kelas' => $faker->randomElement($array = array ('1','2','3'), $count = 1),
            'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
            'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
             
          ]);

        User::create([
            'username' => $siswa->nis,
            'ket' => 'siswa',
            'password' => Hash::make('123123'),
            'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
            'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
          ]);

      }

    }
}

