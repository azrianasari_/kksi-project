<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SiswaSeeder::class);
        $this->call(PegawaiSeeder::class);
        $this->call(MapelSeeder::class);
        $this->call(JadwalSeeder::class);
        $this->call(DaftarKelasSeeder::class);
    
    }
}
