<?php

use Illuminate\Database\Seeder;

class MapelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \App\Mapel::insert([
            [
              'kode' =>'1A',
              'nama' =>'Pendidikan Agama Islam dan Budi Pekerti',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
           
            ],
            [
              'kode' =>'2A',
              'nama' =>'Pendidikan Agama Kristen dan Budi Pekerti',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            
            ],
            [
              'kode' =>'3A',
              'nama' =>'Pendidikan Agama Katolik dan Budi Pekerti',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            
            ],
            [
              'kode' =>'4A',
              'nama' =>'Pendidikan Pancasila dan Kewarganegaraan',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            
            ],
            [
              'kode' =>'6A',
              'nama' =>'Bahasa Indonesia',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            
            ],
            [
              'kode' =>'11A',
              'nama' =>'Matematika',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            
            ],
            [
              'kode' =>'5A2',
              'nama' =>'Sejarah Indonesia',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            
            ],
            [
              'kode' =>'12A',
              'nama' =>'Bahasa Inggris',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            
            ],
            [
              'kode' =>'7B',
              'nama' =>'Seni Budaya',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            
            ],
            [
              'kode' =>'14B',
              'nama' =>'Pendidikan Jasmani, Olahraga, dan Kesehatan',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'kode' =>'10C',
              'nama' =>'Simulasi dan Komunikasi Digital',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'kode' =>'15C',
              'nama' =>'Fisika',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'kode' =>'16C1',
              'nama' =>'Kimia',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'kode' =>'16C2',
              'nama' =>'IPA',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'kode' =>'17C2',
              'nama' =>'Sistem Komputer',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'kode' =>'17C1',
              'nama' =>'Komputer dan Jaringan Dasar',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'kode' =>'19C1',
              'nama' =>'Pemrograman Dasar',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'kode' =>'8C',
              'nama' =>'Dasar Desain Grafis',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
            [
              'kode' =>'25C2',
              'nama' =>'Ekonomi Bisnis',
              'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
              'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ],
          
            ]);
    }
}

