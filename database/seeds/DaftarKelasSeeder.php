<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Pegawai;
use App\Jadwal;
use App\Siswa;
use App\Absensi;
use App\Kelas;


class DaftarKelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //'tanggal','waktu', 'kd', 'materi', 'foto', 'jadwal_id', 'pegawai_id'
        $faker = Faker::create('id');
        $jadwal = Jadwal::get()->pluck('id');
        $pegawai = Pegawai::get()->pluck('id');

        for($i = 1; $i <= 50; $i++){
 
        // insert data ke table pegawai menggunakan Faker
        $daftar_kelas = Kelas::create([
               'tanggal'=> $faker->date($format = 'Y-m-d', $max = 'now'),
               'waktu' => $faker->time($format = 'H:i:s', $max = 'now'),
               'kd' => $faker->realText(rand(10,20)),
               'materi' => $faker->realText(rand(10,20)),
               'jadwal_id' => $jadwal_id = $faker->randomElement($array = $jadwal, $count = 1),           
               'pegawai_id'=>$faker->randomElement($array = $pegawai, $count = 1),
               'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
               'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
             ]);
            //dapatkan data kelas dari tabel jadwal
            $kelas = Jadwal::findOrFail( $jadwal_id)->kelas;
        
            //dapatkan data siswa berdasarkan kelas
            $siswa = Siswa::where('kelas', $kelas)->get();
            
            foreach($siswa as $sw) {
                Absensi::insert([
                    'daftar_kelas_id' => $daftar_kelas->id,
                    'siswa_id' => $sw->id,
                    'keterangan' => '4'
                ]);
            }
       

        }
 
    }
}
