<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Pegawai;
use App\User;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id');
        for($i = 1; $i <= 10; $i++){
 
            // insert data ke table pegawai menggunakan Faker
        $pegawai = Pegawai::create([
                'nip' => $faker->numberBetween($min = 1000, $max = 9000),
                'nama' => $faker->name,
                'alamat' => $faker->address,
                'no_telp' => $faker->phoneNumber,
                'level' => $faker->randomElement($array = array ('0','1')),
                'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
                'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
             
          ]);

          User::create([
            'username' => $pegawai->nip,
            'ket' => 'pegawai',
            'password' => Hash::make('123123'),
            'created_at' => \Carbon\Carbon::now('Asia/Makassar'),
            'updated_at' => \Carbon\Carbon::now('Asia/Makassar')
            ]);

      }

        
    }
}
