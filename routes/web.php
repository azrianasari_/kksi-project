
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
  
Route::get('/dashboard','HomeController@index');

Route::resource('/siswa','SiswaController');

Route::resource('/pegawai','PegawaiController');

Route::resource('/mapel','MapelController');

Route::resource('/jadwal','JadwalController');

Route::resource('/kelas','KelasController');
Route::put('kelas/upload/{id}','KelasController@uploadImage')->name('kelas.upload');
Route::get('/list-kelas','KelasController@getKelas')->name('list.kelas');

Route::resource('/absensi','AbsensiController');
Route::get('/absensi/cetak/{id}', 'AbsensiController@cetak')->name('absensi.cetak');
Route::get('/laporan','AbsensiController@laporan')->name('laporan.index');
Route::get('/laporan/list-kelas','AbsensiController@getKelas')->name('laporan.list.kelas');
Route::get('/absensi/rekap/{id}', 'AbsensiController@cetakRekap')->name('absensi.rekap');

Route::get('siswa/reset/{id}','SiswaController@reset');
Route::get('pegawai/reset/{id}','PegawaiController@reset');

//guru login
Route::get('/daftar/kelas','KelasController@kelasPegawai')->name('kelas.pegawai');
Route::get('/daftar/laporan','AbsensiController@laporanPegawai')->name('laporan.pegawai');


});